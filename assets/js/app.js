var trailist = angular.module('trailist', ['ui.state', 'ui.bootstrap'])

trailist
.config(function($stateProvider, $urlRouterProvider, $locationProvider){

	$urlRouterProvider.otherwise('/home');

	$stateProvider
        /*
            Homepage routes
        */
		.state('index', {
			url: '/',
			templateUrl: 'templates/index.html',
			controller: 'IndexController'
		})
        .state('index.home', {
			url: 'home',
			templateUrl: 'templates/home.html',
			controller: 'IndexController'
		})
        .state('index.trails', {
			url: 'trails',
			templateUrl: 'templates/trails.html',
			controller: 'IndexController'
		})
        .state('logout', {
			url: '/logout',
			templateUrl: 'templates/logout.html',
			controller: 'LoginController'
		})
        .state('logout.home', {
			url: '/',
			templateUrl: 'templates/logout.home.html',
			controller: 'LoginController'
		})
        .state('register', {
			url: '/register',
			templateUrl: 'templates/register.html',
			controller: 'IndexController'
		})
    
        /* User Routes */
    
        .state('user', {
			url: '/user',
			templateUrl: 'templates/user.html',
			controller: 'IndexController'
		})
        .state('user.home', {
			url: '/',
			templateUrl: 'templates/user/home.html',
			controller: 'IndexController'
		})
        .state('user.home.activity', {
			url: 'activity',
			templateUrl: 'templates/user/activity.html',
			controller: 'IndexController'
		})
        .state('user.home.about', {
			url: 'about',
			templateUrl: 'templates/user/about.html',
			controller: 'IndexController'
		})
        .state('user.home.followers', {
			url: 'followers',
			templateUrl: 'templates/user/followers.html',
			controller: 'IndexController'
		})
        .state('user.home.following', {
			url: 'following',
			templateUrl: 'templates/user/following.html',
			controller: 'IndexController'
		})
        .state('user.products', {
			url: '/products',
			templateUrl: 'templates/user/products.html',
			controller: 'IndexController'
		})
        .state('user.products.home', {
			url: '/',
			templateUrl: 'templates/user/products/home.html',
			controller: 'IndexController'
		})
        .state('user.products.sold', {
			url: '/sold',
			templateUrl: 'templates/user/products/sold.html',
			controller: 'IndexController'
		})
        .state('user.products.expired', {
			url: '/expired',
			templateUrl: 'templates/user/products/expired.html',
			controller: 'IndexController'
		})
        
        /* Profile Routes  */
    
		.state('profile', {
			url: '/profile',
			templateUrl: 'templates/profile.html',
			controller: 'IndexController'
		})
        .state('profile.home', {
			url: '/',
			templateUrl: 'templates/profile/home.html',
			controller: 'IndexController'
		})
        .state('profile.home.activity', {
			url: 'activity',
			templateUrl: 'templates/profile/activity.html',
			controller: 'IndexController'
		})
        .state('profile.home.about', {
			url: 'about',
			templateUrl: 'templates/profile/about.html',
			controller: 'IndexController'
		})
        .state('profile.home.followers', {
			url: 'followers',
			templateUrl: 'templates/profile/followers.html',
			controller: 'IndexController'
		})
        .state('profile.home.following', {
			url: 'following',
			templateUrl: 'templates/profile/following.html',
			controller: 'IndexController'
		})
        .state('profile.messages', {
			url: '/messages',
			templateUrl: 'templates/profile/messages.html',
			controller: 'MessagesController'
		})
        .state('profile.messages.home', {
			url: '/',
			templateUrl: 'templates/profile/messages/home.html',
			controller: 'MessagesController'
		})
        .state('profile.messages.home.detail', {
			url: 'detail',
			templateUrl: 'templates/profile/messages/detail.html',
			controller: 'MessagesController'
		})
        .state('profile.products', {
			url: '/products',
			templateUrl: 'templates/profile/products.html',
			controller: 'IndexController'
		})
        .state('profile.products.home', {
			url: '/',
			templateUrl: 'templates/profile/products/home.html',
			controller: 'IndexController'
		})
        .state('profile.products.sold', {
			url: '/sold',
			templateUrl: 'templates/profile/products/sold.html',
			controller: 'IndexController'
		})
        .state('profile.products.expired', {
			url: '/expired',
			templateUrl: 'templates/profile/products/expired.html',
			controller: 'IndexController'
		})
        .state('profile.transactions', {
			url: '/transactions',
			templateUrl: 'templates/profile/transactions.html',
			controller: 'MessagesController'
		})
        .state('profile.transactions.home', {
			url: '/',
			templateUrl: 'templates/profile/transactions/home.html',
			controller: 'MessagesController'
		});
    
    $locationProvider
                      .html5Mode(false)
                      .hashPrefix('!');
});