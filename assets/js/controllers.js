trailist.controller('IndexController', function($scope, $timeout, $stateParams) {
    
    $('#main-menu').css('display', 'block');
    $('#main-settings-menu').css('display', 'block');

    $('#home-feed-list').masonry({
        itemSelector: '.post-box',
        isAnimated: true,
        animationOptions: {
            duration: 750,
            easing: 'linear',
            queue: false
        }
    }); 

    $('.reply-btn').click(function(e) {

        e.preventDefault();

        $(this).parent().parent().find('.item-footer').slideToggle(20, function(e) {
            $timeout(function(){ 
                $('#home-feed-list').masonry('destroy') 
            }, 30);

            $timeout(function(){ 
                $('#home-feed-list').masonry() 
            }, 200);
        });
    });

    $('#sort-menu-btn').click(function(e) {
        e.preventDefault();

        $('#sort-filters').slideToggle(200);
    });

/*

    $('html').click(function() {
        $('body').bind('click', function(e) {
            if($(e.target).closest('.post-box').length == 0) {
                $('.item-footer').slideUp(20, function() {

                    $timeout(function(){ 
                        $('#home-feed-list').masonry('destroy') 
                    }, 30);

                    $timeout(function(){ 
                        $('#home-feed-list').masonry() 
                    }, 200);
                });
            }
        });
    });

*/
    
    $('#sell-btn').click(function(e) {
        e.preventDefault();
        
        $('#buy-form').hide();
        $('#sell-form').fadeIn();
        
        $('#buy-btn').removeClass('active');
        $('#sell-btn').addClass('active');
        
    });
    
    $('#buy-btn').click(function(e) {
        e.preventDefault();
        
        $('#sell-form').hide();
        $('#buy-form').fadeIn();
        
        $('#sell-btn').removeClass('active');
        $('#buy-btn').addClass('active');
        
    });

    $('.post-textarea').focus(function() {
        $(this).parent().parent().find('.reply-tools').slideToggle();
    });

});

trailist.controller('LoginController', function($scope, $timeout, $stateParams) {
    
    $('#main-menu').css('display', 'none');
    $('#main-settings-menu').css('display', 'none');
    
    $('.cancel-btn').click(function() {
        $(this).parent().parent().parent().find('.item-footer').slideToggle();
    });
    
    $('#sell-btn').click(function(e) {
        e.preventDefault();
        
        $('#buy-form').hide();
        $('#sell-form').fadeIn();
        
        $('#buy-btn').removeClass('active');
        $('#sell-btn').addClass('active');
        
    });
    
    $('#buy-btn').click(function(e) {
        e.preventDefault();
        
        $('#sell-form').hide();
        $('#buy-form').fadeIn();
        
        $('#sell-btn').removeClass('active');
        $('#buy-btn').addClass('active');
        
    });
    
    var tour = new Tour({
        name: "tour",
        steps: [
            {
                element: "#facebook-connect",
                title: "Title of my step",
                content: "Content of my step"
            },
            {
                element: "#my-other-element",
                title: "Title of my step",
                content: "Content of my step"
            }
        ],
        container: "body",
        keyboard: true,
        storage: window.localStorage,
        debug: false,
        backdrop: true,
        redirect: true,
        orphan: false,
        duration: false,
        basePath: "",
        afterGetState: function (key, value) {},
        afterSetState: function (key, value) {},
        afterRemoveState: function (key, value) {},
        onStart: function (tour) {},
        onEnd: function (tour) {},
        onShow: function (tour) {},
        onShown: function (tour) {
        },
        onHide: function (tour) {
        },
        onHidden: function (tour) {},
        onNext: function (tour) {},
        onPrev: function (tour) {},
        onPause: function (tour, duration) {},
        onResume: function (tour, duration) {}
    });
    $('#tour').click(function(){
        
        // Start the tour
        tour.start();

    });

});

trailist.controller('MessagesController', function($scope, $timeout, $stateParams) {

	var windowHeight = $(window).height();
    
    $('#message-container').css('height', windowHeight - 130);
    
    var messageContainer = $('#message-container').height();
    
    var messageDetailInner = $('#message-detail-inner').height();
    
    var messageFooter = $('#message-footer').height();
    
    $('#message-detail-inner').height(messageContainer - 20);
    
    $('#message-content').css('height', messageContainer - messageFooter - 30);
    
    $('#pm-list').css('height', messageContainer);
    
    $('#message-detail').css('height', messageContainer);

});