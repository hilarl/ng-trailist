heyMaldives.controller('IndexController', function($scope, $timeout, $stateParams) {
	
	$.backstretch([
      "http://www.wwwmaldives.com/wp-content/uploads/Sheraton-Maldives-Ocean-Villa.jpg"
    , "http://www.shangri-la.com/uploadedImages/Shangri-La_Resorts/Shangri-La%E2%80%99s_Villingili_Resort_and_Spa,_Maldives/SLMD-Bg-Bird-View-v2.jpg"
    , "http://www.hdwallpapers.in/walls/maldives_islands-wide.jpg"
    , "http://d1vmp8zzttzftq.cloudfront.net/wp-content/uploads/2012/05/coral-reef-atolls-in-maldives-1600x1066.jpg"
  ], {duration: 3000, fade: 750});

	$('.slider').slider();

});

heyMaldives.controller('ExploreController', function($scope, $timeout, $stateParams) {

	$('#main-carousel').carousel({
		interval: 7000
	});

});