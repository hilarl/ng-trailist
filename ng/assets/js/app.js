var heyMaldives = angular.module('heyMaldives', ['ui.state', 'ui.bootstrap'])

heyMaldives.value('$anchorScroll', angular.noop)
.config(function($stateProvider, $urlRouterProvider){

	$urlRouterProvider.otherwise('/')

	$stateProvider
		.state('/', {
			url: '/',
			templateUrl: 'templates/index.html',
			controller: 'IndexController'
		})
		.state('bookHoliday', {
			url: '/book',
			templateUrl: 'templates/book.html',
			controller: 'IndexController'
		})
		.state('explore', {
			url: '/explore',
			templateUrl: 'templates/explore.html',
			controller: 'ExploreController'
		})
			.state('explore.home', {
				url: '/',
				templateUrl: 'templates/home.html',
				controller: 'ExploreController'
			})
})